﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace DrawShape
{

    /// <summary>
    /// User`s drawing. Re-created every time, when user touched screen.
    /// Note, that all points calculations performed in screen space.
    /// </summary>
    [Serializable]
    public class Drawing
    {
        #region PrivateMembers
        
        private List<Vector2> m_pathPoints = new List<Vector2>();                    //for tracking input - adds new PATH point on every pointer move;
        private List<CornerPoint> m_cornerPointsPotential = new List<CornerPoint>();     // POTENTIAL point added there when pointer changes direction
        private List<CornerPoint> m_cornerPoints = new List<CornerPoint>();              //  CORNER point added there few times after POTENTIAL - when it will be checked, is direction realy changed, or it was a little roughness.
        private List<CornerPoint> m_finalCorners = new List<CornerPoint>();               //After drawing finished, there will be added WELDED CORNER points (to convert rounded corners into single points)

        private bool analized = false;  //Flag used to check, is m_finalCorners list already filled with analized corner points.

        #endregion

       #region Input

        //Create new drawing
        public Drawing(TouchSimulation simulatedTouch, Shape currentTemplate)
        {
            AddPathPoint(simulatedTouch.Position);
            AddCornerPoint(simulatedTouch.Position);
        }

        //Called when drawing line was closed
        public void DrawingLineClosed()
        {
            AnalizeFinalCorners();
            Vector3[] result = new Vector3[m_finalCorners.Count];
            if (m_finalCorners.Count > 2)
            {
                for (int i = 0; i < m_finalCorners.Count; i++)
                {
                    result[i] = new Vector3(m_finalCorners[i].position.x, 0, m_finalCorners[i].position.y);
                    FurniturePlacingTool.Room.DrawedPlan = result;
                    Application.LoadLevel(0);
                }
            }
            else
            {
                //Application.LoadLevel(Application.loadedLevel);
            }

        }

        /// <summary>
        /// Update drawing with new input.
        /// Note: this is not a MonoBehaviour`s child, must be updated manually.
        /// </summary>
        /// <param name="simulatedTouch">Input</param>
        internal void Update(TouchSimulation simulatedTouch)
        {
            if (!analized)  //Update only not finished drawings.
            {
                // if m_pathPoints not Null AND there is enough path points for at least 3 corners with minimal distance btw them AND current point equals start point.
                if (m_pathPoints != null && m_pathPoints.Count > RecognitionSettings.minPathPointsNumberForCheckCorners * 3 && IsPointsDifferent(simulatedTouch.Position, m_pathPoints[0]) == false) //END drawing and analize
                {
                    simulatedTouch.Phase = TouchPhase.Ended;
                    DrawingLineClosed(); 
                }
                else
                {
                    if (simulatedTouch.Phase == TouchPhase.Ended)   //RESET drawing
                    {
                        if (m_pathPoints.Count > 3)
                        {
                            Application.LoadLevel(Application.loadedLevel);
                        }
                    }
                    else //DRAW
                    {
                        //Track every pointer motion with point:
                        AddPathPoint(simulatedTouch.Position);

                        //As far as we can`t predict where pointer will be moved, add potential corner point every time, when pointer`s direction isn`t equal direction from previous point to pointer.
                        CheckCornersPotential(simulatedTouch);

                        //Then check previously added points (far enough from current pointer position for check direction change)
                        CheckCorners(simulatedTouch);
                    }
                    
                }
            }
        }

        #endregion

        #region ProcessInput

        /// <summary>
        /// Check, is it necessary to add new corner point at this step.
        /// Called every update.
        /// </summary>
        /// <param name="simulatedTouch"></param>
        private void CheckCorners(TouchSimulation simulatedTouch)
        {
            List<CornerPoint> pointsDone = new List<CornerPoint>();

            // iterate potential points
            foreach (CornerPoint point in m_cornerPointsPotential)
            {
                if (point.awaitingForPoint == m_pathPoints.Count - 1)   //if this potential point was added N points before
                {
                    //check, is there a direction change (is angles different) btw current point, point added N times before, and point added Nx2 times before
                    if (IsCorner(m_pathPoints[point.awaitingForPoint - (RecognitionSettings.minPathPointsNumberForCheckCorners * 2)], point.position, m_pathPoints[point.awaitingForPoint]))
                    {
                        AddCornerPoint(point.position); // there is a direction change - save this point as corner.
                    }
                    // We cant remove this point as far as we still analizing another points, so
                    // Save current point to separate array:
                    pointsDone.Add(point);
                }
            }

            //After all checks, remove checked point from array:
            foreach (CornerPoint point in pointsDone)
            {
                m_cornerPointsPotential.Remove(point);
            }
        }

        /// <summary>
        /// Check, is it necessary to add new potential corner point at this step.
        /// Called every update.
        /// </summary>
        /// <param name="simulatedTouch"></param>
        private void CheckCornersPotential(TouchSimulation simulatedTouch)
        {

            if (m_pathPoints.Count >= RecognitionSettings.minPathPointsNumberForCheckCorners)   //if there is enough path points for corner
            {
                Vector2 previousPathPoint = m_pathPoints[m_pathPoints.Count - RecognitionSettings.minPathPointsNumberForCheckCorners];    // get path point N steps before current point for check
                if (Vector2.Distance(previousPathPoint, simulatedTouch.Position) >= RecognitionSettings.minDistanceBtwPathPoints * 2)     //is distance btw current and previous point enough for check angle
                {
                    if (IsCorner(previousPathPoint, simulatedTouch.Position, simulatedTouch.Position + simulatedTouch.DeltaPosition))  //is there direction change btw previous direction and current touch motion
                    {
                        AddPotentialCornerPoint(simulatedTouch.Position); //save this point for fruther (after N points will be added) check
                    }
                }
            }
        }



        #endregion

        /// <summary>
        /// As far as drawing corners are smooth, weld nearest corner points to have a single-point corners and straight lines between them as drawing result.
        /// Called after drawing was finished (drawing line closed).
        /// </summary>
        private void AnalizeFinalCorners()
        {
            //Encapsulate all corner points inside single bounds for get drawing size
            Bounds figureBounds = new Bounds(m_cornerPoints[0].position, Vector3.zero);
            for (int i = 1; i < m_cornerPoints.Count; i++)
            {
                figureBounds.Encapsulate(m_cornerPoints[i].position);
            }

            // define welding treshold
            float treshold = Mathf.Min(figureBounds.size.x, figureBounds.size.y) / RecognitionSettings.weldingTreshold;

            //weld points by treshold
            Vector2 currentFinalCornerPoint = m_cornerPoints[0].position; //current point is a first point in array
            for (int j = 0; j < m_cornerPoints.Count - 1; j++)
            {
                if (Vector2.Distance(currentFinalCornerPoint, m_cornerPoints[j + 1].position) > treshold || j >= m_cornerPoints.Count - 2) //if distance btw current point and next point large than treshold, OR current point does not have next point in array
                {
                    //there is a straight line segment - add corner point
                    AddFinalCorner(currentFinalCornerPoint);
                    currentFinalCornerPoint = m_cornerPoints[j + 1].position; // Check completed - Set next point as current
                }
                else
                {
                    //there is a corner - move current corner point to the middle position of current group of smooth corner points.
                    currentFinalCornerPoint = Vector2.Lerp(currentFinalCornerPoint, m_cornerPoints[j + 1].position, 0.5f);
                }
            }
        }

       

        #region AddPointsRoutines

        /// <summary>
        /// Add new path point when pointer is far enough from previous path point
        /// </summary>
        /// <param name="pointerPosition"></param>
        private void AddPathPoint(Vector2 pointerPosition)
        {
            if (m_pathPoints.Count == 0 || Vector2.Distance(m_pathPoints[m_pathPoints.Count - 1], pointerPosition) > RecognitionSettings.minDistanceBtwPathPoints)
            {
                m_pathPoints.Add(pointerPosition);
            }
          
        }

        private void AddCornerPoint(Vector2 pointerPosition)
        {
            m_cornerPoints.Add(new CornerPoint(pointerPosition));
        }

        private void AddFinalCorner(Vector2 pointerPosition)
        {
            m_finalCorners.Add(new CornerPoint(pointerPosition));
        }

        private void AddPotentialCornerPoint(Vector2 pointerPosition)
        {
            m_cornerPointsPotential.Add(new CornerPoint(pointerPosition, m_pathPoints.Count + RecognitionSettings.minPathPointsNumberForCheckCorners));
        }


        #endregion

        #region Utility

        /// <summary>
        /// Are given three points makes triangle, or just straight line?
        /// </summary>
        /// <param name="firstPoint"></param>
        /// <param name="secondPoint"></param>
        /// <param name="thirdPoint"></param>
        /// <returns></returns>
        private bool IsCorner(Vector2 firstPoint, Vector2 secondPoint, Vector2 thirdPoint)
        {
            Vector2 firstVector = secondPoint - firstPoint;
            Vector2 secondVector = thirdPoint - secondPoint;
            float angle = Vector2.Angle(firstVector, secondVector);
            if (firstVector.magnitude > RecognitionSettings.motionTreshold && secondVector.magnitude > RecognitionSettings.motionTreshold)
            {
                if (angle > RecognitionSettings.angularTreshold)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Is two points far enough from each other?
        /// </summary>
        /// <param name="first"></param>
        /// <param name="second"></param>
        /// <returns></returns>
        private bool IsPointsDifferent(Vector2 first, Vector2 second)
        {
            if (Vector2.Distance (first, second) >= RecognitionSettings.fingerWidth)
            {
                return true;
            }
            return false;
        }

        #endregion
    }
}
