﻿using UnityEngine;
using System.Collections;

namespace DrawShape
{
    /// <summary>
    /// Corner point
    /// </summary>
    [System.Serializable]
    public struct CornerPoint
    {
        public Vector2 position;
        public int awaitingForPoint;

        public CornerPoint (Vector2 pos, int waitingForPathPoint = 0)
        {
            position = pos;
            awaitingForPoint = waitingForPathPoint;
        }

    }
}
