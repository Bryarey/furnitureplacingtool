﻿using UnityEngine;
using UnityEngine.UI;


namespace FurniturePlacingTool
{
    /// <summary>
    /// Represents visual world-space preview, that shows current pointer projection on selected wall by vertical and horisontal lines.
    /// </summary>
    public class PlacementPreview : MonoBehaviour
    {
        [SerializeField]
        private RectTransform m_placementHorizontalLine;

        [SerializeField]
        private RectTransform m_placementVerticalLine;

        [SerializeField]
        private RectTransform m_distanceMeasurement;

        private Text m_distanceValue;

        /// <summary>
        /// Static creator.
        /// </summary>
        /// <param name="bottomLeft"></param>
        /// <param name="bottomRight"></param>
        /// <param name="topLeft"></param>
        /// <param name="topRight"></param>
        /// <returns></returns>
        static public PlacementPreview Create(Vector3 bottomLeft, Vector3 bottomRight, Vector3 topLeft, Vector3 topRight)
        {

            Vector3 midPosition = Vector3.Lerp(bottomLeft, topRight, 0.5f);

            GameObject newObject = Instantiate(Resources.Load("Prefabs/PlacementPreview") as GameObject, midPosition, Quaternion.identity) as GameObject;

            newObject.transform.position = midPosition; //Set position

            newObject.transform.right = Vector3.Normalize(bottomLeft - bottomRight); //set rotation

            //Set scale:
            Vector3 scale = Vector3.one;
            scale.x = Vector3.Distance(bottomLeft, bottomRight);
            scale.y = Vector3.Distance(bottomLeft, topLeft);
            newObject.transform.localScale = scale;

            return newObject.GetComponent<PlacementPreview>();
        }

        /// <summary>
        /// Set value for horizontal line (useful for placing objects, that not anchored to floor, e.g. mirrors or paintings)
        /// </summary>
        /// <param name="normalizedHeight"></param>
        public void SetPlacementLine(float normalizedHeight)
        {
            Vector2 anchorMax = m_placementHorizontalLine.anchorMax;
            anchorMax.y = normalizedHeight;
            m_placementHorizontalLine.anchorMax = anchorMax;

            Vector2 anchorMin = m_placementHorizontalLine.anchorMin;
            anchorMin.y = normalizedHeight;
            m_placementHorizontalLine.anchorMin = anchorMin;
        }

        /// <summary>
        /// Set value for vertical line and distance measurement.
        /// </summary>
        /// <param name="normalizedPosition"> normalized position of vertical line</param>
        /// /// <param name="distance">distance from first corner in units (meters)</param>
        public void SetPlacementPoint(float normalizedPosition, float distance)
        {
            Vector2 anchorMax = m_placementVerticalLine.anchorMax;
            anchorMax.x = normalizedPosition;
            m_placementVerticalLine.anchorMax = anchorMax;

            Vector2 anchorMin = m_placementVerticalLine.anchorMin;
            anchorMin.x = normalizedPosition;
            m_placementVerticalLine.anchorMin = anchorMin;
            m_distanceMeasurement.anchorMin = new Vector2(anchorMin.x, m_distanceMeasurement.anchorMin.y);

            if (m_distanceValue == null)
            {
                m_distanceValue = gameObject.GetComponentInChildren<Text>();
            }

            m_distanceValue.text = "<< " + distance.ToString() + " >>";


        }
    }
}