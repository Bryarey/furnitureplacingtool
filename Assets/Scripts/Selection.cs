﻿using UnityEngine;
using UnityEngine.EventSystems;
using FurniturePlacingTool;

namespace FurniturePlacingTool
{
    /// <summary>
    /// Enum represents possible states.
    /// </summary>
    public enum SelectionState { SelectProduct, SelectFirstCorner, SelectSecondCorner, SetProductPosition };

    /// <summary>
    /// Performs all selection/deselection operations, selection previews, and dealing with selected objects, manages program`s states.
    /// </summary>
    public class Selection : MonoBehaviour
    {
        #region PrivateMembers
        //current program state
        private SelectionState m_selectionState = SelectionState.SelectProduct;

        // there will be stored edge selection container prefab
        private GameObject m_selectionFramePrefab;

        // width of edge selection containers
        private float m_selectionFrameWidth = 0.1f;

        //current edge selection frame
        private GameObject m_selectionPreview;

        //Current world-space canvas preview
        private PlacementPreview m_placementPreview;
        

        //Selected edges:
        private Edge m_firstSelectedEdge;   //first selected
        private Edge m_secondSelectedEdge;  //second selected
        private Edge m_edgeUnderCursor;     //highlighted

        //Selected product
        private Product m_selectedProduct;

        // current raycast hit (mouse position -> ray from camera -> wall collider) world position
        private Vector3 m_currentMousePosition;

        //static instance for fast Find()
        static private Selection m_instance;

        //property loads prefab from resources on first call
        private GameObject SelectionFramePrefab
        {
            get
            {
                if (m_selectionFramePrefab == null)
                {
                    m_selectionFramePrefab = Resources.Load("Prefabs/SelectionFrame") as GameObject;
                }
                return m_selectionFramePrefab;
            }
        }

        #endregion

        #region PublicProperties

        public Vector3 CurrentMousePosition
        {
            get
            {
                return m_currentMousePosition;
            }

            set
            {
                m_currentMousePosition = value;
            }
        }

        //Automatically updates UI state label when changed
        public SelectionState SelectionState
        {
            get
            {
                return m_selectionState;
            }

            set
            {
                if (value != m_selectionState)
                {
                    UI.Find().UpdateState(value.ToString());
                }
                m_selectionState = value;
            }
        }

        #endregion

        /// <summary>
        /// Quick finder
        /// </summary>
        /// <returns></returns>
        public static Selection Find()
        {
            return m_instance;
        }

        #region MonoBehaviourMessages

        private void Awake()
        {
            m_instance = this;  //set up finder
        }

        private void Update()
        {
            //State machine
            switch (SelectionState)
            {
                case SelectionState.SelectProduct:
                    break;
                case SelectionState.SelectFirstCorner:
                    SelectingFirstCorner();
                    break;
                case SelectionState.SelectSecondCorner:
                    SelectingSecondCorner();
                    break;
                case SelectionState.SetProductPosition:
                    DefineProductPosition();
                    break;
            }
        }

        #endregion

        #region StatesProcessing

        /// <summary>
        /// User celects first corner.
        /// </summary>
        private void SelectingFirstCorner()
        {
            if (Input.GetMouseButtonDown(0))
            {
                if (m_edgeUnderCursor != null)
                {
                    //Selected
                    m_firstSelectedEdge = m_edgeUnderCursor;
                    SelectionState = SelectionState.SelectSecondCorner;
                }
                else
                {
                    //Click on empty space - deselect product;
                    ClearSelection();
                    SelectionState = SelectionState.SelectProduct;
                }
            }
        }

        /// <summary>
        /// User selects second corner (or cancels selection).
        /// </summary>
        private void SelectingSecondCorner()
        {
            if (Input.GetMouseButtonDown(0))
            {
                //select second corner or clear selection
                if (m_edgeUnderCursor != null)
                {
                    //Is ajacent edges selected?
                    int indexDifference = Mathf.Abs(m_edgeUnderCursor.PlanPointIndex - m_firstSelectedEdge.PlanPointIndex);
                    if (indexDifference == 1 || indexDifference == Room.Find().PlanPoints.Length - 1)
                    {
                        //select second corner
                        m_secondSelectedEdge = m_edgeUnderCursor;

                        // Check selection "Normal", and mirror selection, is necessary
                        CheckSelectionOrder();

                        m_placementPreview = PlacementPreview.Create(m_firstSelectedEdge.FirstPoint, m_secondSelectedEdge.FirstPoint, m_firstSelectedEdge.SecondPoint, m_secondSelectedEdge.SecondPoint);
                        SelectionState = SelectionState.SetProductPosition;
                    }
                    else
                    {
                        Debug.LogWarning("Incorrect edge! Please select adjacent edges!");
                        ClearSelection();
                        SelectionState = SelectionState.SelectProduct;
                    }
                }
                else
                {
                    ClearSelection();
                    SelectionState = SelectionState.SelectProduct;
                }
            }
        }

        /// <summary>
        /// User defines product`s new position
        /// </summary>
        private void DefineProductPosition()
        {
            UpdateProductPosition();

            //Is position defined?
            if (Input.GetMouseButtonDown(0))
            {
                //if click not on UI input field
                if (!EventSystem.current.IsPointerOverGameObject())
                {
                    //product placement finished
                    ClearSelection();
                    SelectionState = SelectionState.SelectProduct;
                }
            }
        }

        /// <summary>
        /// Define current placement position and move product accordingly.
        /// </summary>
        private void UpdateProductPosition()
        {
            //Prevent product move out of selected wall - subtract product width from placement line
            float productHalfSize = m_selectedProduct.transform.localScale.x / 2;   
            Vector3 clampedStart = Vector3.MoveTowards(m_firstSelectedEdge.FirstPoint, m_secondSelectedEdge.FirstPoint, productHalfSize);
            Vector3 clampedEnd = Vector3.MoveTowards(m_secondSelectedEdge.FirstPoint, m_firstSelectedEdge.FirstPoint, productHalfSize);

            //Define placement point
            Vector3 placementPoint = ProjectPointOnLineSegment(clampedStart, clampedEnd, CurrentMousePosition);

            //Set product position
            m_selectedProduct.transform.position = placementPoint;
            m_selectedProduct.transform.right = m_secondSelectedEdge.FirstPoint - m_firstSelectedEdge.FirstPoint;

            //Update UI input field (if no input at this time)
            if (UI.Find().EnteringSize == false)
            {
                UI.Find().InputField.text = Vector3.Distance(m_firstSelectedEdge.FirstPoint, placementPoint).ToString();
            }

            //update UI range label
            float distance = Vector3.Distance(m_firstSelectedEdge.FirstPoint, placementPoint);
            UI.Find().UpdateRange(Vector3.Distance(m_firstSelectedEdge.FirstPoint, clampedStart), Vector3.Distance(m_firstSelectedEdge.FirstPoint, clampedEnd));

            //Update world-space canvas preview
            UpdatePlacementPreview(placementPoint, distance);
        }

        /// <summary>
        /// Update world-space canvas, that shows vertical and horizontal pointer position on selected wall.
        /// </summary>
        /// <param name="placementPoint">current placement point</param>
        /// <param name="distance">distance in meters from first corner to vertical line</param>
        private void UpdatePlacementPreview(Vector3 placementPoint, float distance)
        {
            //horizontal line
            m_placementPreview.SetPlacementLine(Mathf.InverseLerp(m_firstSelectedEdge.FirstPoint.y, m_firstSelectedEdge.SecondPoint.y, CurrentMousePosition.y));
            //vertical line
            m_placementPreview.SetPlacementPoint(Mathf.InverseLerp(Vector3.Distance(m_firstSelectedEdge.FirstPoint, m_secondSelectedEdge.FirstPoint), 0f, m_firstSelectedEdge.GetDistanceToLine(placementPoint)), distance);
        }


        /// <summary>
        /// Select given product.
        /// </summary>
        /// <param name="product">product to select.</param>
        public void SelectProduct(Product product)
        {
            m_selectedProduct = product;
            SelectionState = SelectionState.SelectFirstCorner;
        }



        /// <summary>
        /// Apply value from UI inputfield to current placement. Moves product to specified position and clears selection.
        /// </summary>
        /// <param name="distance">Distance from wall starting point to product</param>
        public void SetValueFromInput(float distance)
        {
            //Prevent product diving into walls (in case of incorrect value was entered)
            float productHalfSize = m_selectedProduct.transform.localScale.x / 2;
            Vector3 clampedStart = Vector3.MoveTowards(m_firstSelectedEdge.FirstPoint, m_secondSelectedEdge.FirstPoint, productHalfSize);
            Vector3 clampedEnd = Vector3.MoveTowards(m_secondSelectedEdge.FirstPoint, m_firstSelectedEdge.FirstPoint, productHalfSize);
            distance = Mathf.Clamp(distance, Vector3.Distance(m_firstSelectedEdge.FirstPoint, clampedStart), Vector3.Distance(m_firstSelectedEdge.FirstPoint, clampedEnd));

            //Define placement point
            Vector3 placementPoint = Vector3.MoveTowards(m_firstSelectedEdge.FirstPoint, m_secondSelectedEdge.FirstPoint, distance);

            m_selectedProduct.transform.position = placementPoint; //move to position
            m_selectedProduct.transform.right = m_secondSelectedEdge.FirstPoint - m_firstSelectedEdge.FirstPoint;   //Set rotation

            ClearSelection();

            SelectionState = SelectionState.SelectProduct;
        }

        #endregion

        #region Utility

        /// <summary>
        /// Clears all selections and destroys all previews.
        /// </summary>
        private void ClearSelection()
        {
            m_selectedProduct.Deselect();
            m_firstSelectedEdge = null;
            m_secondSelectedEdge = null;
            m_selectedProduct = null;
            if (m_placementPreview != null)
            {
                Destroy(m_placementPreview.gameObject);
            }
        }

        /// <summary>
        /// Check, is edges order in selection correct: this affects to selection "forward" vector, i.e. to product and preview orientation. 
        /// Mirror selection, if necessary.
        /// </summary>
        private void CheckSelectionOrder()
        {
            if (m_firstSelectedEdge.PlanPointIndex > m_secondSelectedEdge.PlanPointIndex)
            {
                if (m_secondSelectedEdge.PlanPointIndex != 0 || m_firstSelectedEdge.PlanPointIndex == 1)
                {
                    MirrorSelection();
                }
            }
            else
            {
                if (m_firstSelectedEdge.PlanPointIndex == 0 && m_secondSelectedEdge.PlanPointIndex != 1)
                {
                    MirrorSelection();
                }
            }
        }

        /// <summary>
        /// Flips selected edges.
        /// </summary>
        private void MirrorSelection()
        {
            Edge tempEdge = m_firstSelectedEdge;
            m_firstSelectedEdge = m_secondSelectedEdge;
            m_secondSelectedEdge = tempEdge;
        }

        /// <summary>
        /// Select Edge.
        /// </summary>
        /// <param name="edge">Edge to select</param>
        public void Select(Edge edge)
        {
            CreateSelectionPreview(edge);   //create selection container around edge
            m_edgeUnderCursor = edge;
        }

        /// <summary>
        /// Creates container around selected edge.
        /// </summary>
        /// <param name="edge"></param>
        private void CreateSelectionPreview(Edge edge)
        {
            if (m_selectionPreview == null)
            {
                m_selectionPreview = Instantiate(SelectionFramePrefab) as GameObject;
            }

            m_selectionPreview.transform.position = (edge.FirstPoint + edge.SecondPoint) / 2.0f;

            m_selectionPreview.transform.right = Vector3.Normalize(edge.SecondPoint - edge.FirstPoint);
            Vector3 scale = Vector3.one * m_selectionFrameWidth;
            scale.x = Vector3.Distance(edge.FirstPoint, edge.SecondPoint);
            m_selectionPreview.transform.localScale = scale;
        }

        /// <summary>
        /// Deselect current edge
        /// </summary>
        public void DeselectEdge()
        {
            m_edgeUnderCursor = null;
            Destroy(m_selectionPreview);
        }


        /// <summary>
        /// Taken (and slightly modified) from Unity Wiki: http://wiki.unity3d.com/index.php/3d_Math_functions
        /// </summary>
        /// <param name="linePoint1"></param>
        /// <param name="linePoint2"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        /// 
        //This function returns a point which is a projection from a point to a line segment.
        //If the projected point lies outside of the line segment, the projected point will 
        //be clamped to the appropriate line edge.
        //If the line is infinite instead of a segment, use ProjectPointOnLine() instead.
        public static Vector3 ProjectPointOnLineSegment(Vector3 linePoint1, Vector3 linePoint2, Vector3 point)
        {

            Vector3 vector = linePoint2 - linePoint1;

            Vector3 linePointToPoint = point - linePoint1;
            float t = Vector3.Dot(linePointToPoint, vector.normalized);
            Vector3 projectedPoint = linePoint1 + vector.normalized * t;

            Vector3 pointVec = projectedPoint - linePoint1;
            float dot = Vector3.Dot(pointVec, vector);

            if (dot > 0)
            {
                //point is on the line segment
                if (pointVec.magnitude <= vector.magnitude)
                {
                    return projectedPoint;
                }
                else
                {
                    return linePoint2;
                }
            }
            else
            {
                return linePoint1;
            }

        }

        #endregion
    }
}