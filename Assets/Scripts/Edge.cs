﻿using UnityEngine;


namespace FurniturePlacingTool
{
    /// <summary>
    /// Represents an indexed and directed geometric edge.
    /// </summary>
    public class Edge
    {
        #region PrivateMembers

        private Vector3 m_firstPoint;
        private Vector3 m_secondPoint;
        private int m_planPointIndex;

        #endregion

        #region PublicProperties

        public Vector3 FirstPoint
        {
            get
            {
                return m_firstPoint;
            }

            set
            {
                m_firstPoint = value;
            }
        }
        public Vector3 SecondPoint
        {
            get
            {
                return m_secondPoint;
            }

            set
            {
                m_secondPoint = value;
            }
        }
        public int PlanPointIndex
        {
            get
            {
                return m_planPointIndex;
            }
        }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="first">first point</param>
        /// <param name="second">second point</param>
        /// <param name="index">index in plan`s points array</param>
        public Edge(Vector3 first, Vector3 second, int index)
        {
            m_firstPoint = first;
            m_secondPoint = second;
            m_planPointIndex = index;
        }

        #region Utilites

        /// <summary>
        /// Calculates shortest distance from given point to edge`s line
        /// </summary>
        /// <param name="measuringPoint">given point</param>
        /// <returns>float distance</returns>
        public float GetDistanceToLine(Vector3 measuringPoint)
        {
            Vector3 lineDirection = m_secondPoint - m_firstPoint;
            return Vector3.Cross(lineDirection.normalized, measuringPoint - m_firstPoint).magnitude;
        }

        /// <summary>
        /// Returns point on segment, nearest to given. Segment is clamped by start and end points.
        /// </summary>
        /// <param name="measuringPoint">given point in space</param>
        /// <returns>point on segment</returns>
        public Vector3 GetNearestPoint(Vector3 measuringPoint)
        {
            Vector3 vector = m_secondPoint - m_firstPoint;

            Vector3 linePointToPoint = measuringPoint - m_firstPoint;

            float t = Vector3.Dot(linePointToPoint, vector);

            Vector3 projectedPoint = m_firstPoint + vector * t;

            projectedPoint.y = Mathf.Clamp(projectedPoint.y, m_firstPoint.y, m_secondPoint.y);

            return projectedPoint;
        }

        #endregion

    }
}