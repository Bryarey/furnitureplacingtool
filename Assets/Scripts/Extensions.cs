﻿using UnityEngine;
using System.Collections;

public static class Extensions
{

	static public Vector3 Clamp(this Vector3 point, Vector3 min, Vector3 max)
    {
        point.x = Mathf.Clamp(point.x, min.x, max.x);
        point.y = Mathf.Clamp(point.y, min.y, max.y);
        point.z = Mathf.Clamp(point.z, min.z, max.z);
        return point;
    }
}

