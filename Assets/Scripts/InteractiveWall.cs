﻿using UnityEngine;
using System.Collections;

namespace FurniturePlacingTool
{
    /// <summary>
    /// Represents wall object, that us to find nearest (to a particular point) corner.
    /// Most important ting in wall object is a MeshCollider component, that allows us to get exact raycast hit point.
    /// </summary>
    public class InteractiveWall : MonoBehaviour
    {
        #region PrivateMembers

        private MeshFilter m_meshFilter;
        private MeshCollider m_meshCollider;
        private Edge[] m_corners;

        #endregion

        #region PublicProperties

        public MeshFilter MeshFilter
        {
            get
            {
                return m_meshFilter;
            }

            set
            {
                m_meshFilter = value;
            }
        }

        public MeshCollider MeshCollider
        {
            get
            {
                return m_meshCollider;
            }

            set
            {
                m_meshCollider = value;
            }
        }

        public Edge[] Corners
        {
            get
            {
                return m_corners;
            }

            set
            {
                m_corners = value;
            }
        }

        #endregion

        /// <summary>
        /// defines nearest corner
        /// </summary>
        /// <param name="point">given point</param>
        /// <returns>nearest corner</returns>
        public Edge GetNearestCorner(Vector3 point)
        {
            if (Corners[0].GetDistanceToLine(point) < Corners[1].GetDistanceToLine(point))
            {
                return Corners[0];
            }
            else
            {
                return Corners[1];
            }
        }


    }
}