﻿using UnityEngine;

namespace FurniturePlacingTool
{
    /// <summary>
    /// Represents products (furnishing and devices) to dealing with.
    /// </summary>
    public class Product : MonoBehaviour
    {
        private Collider m_collider;

        private void Awake()
        {
            m_collider = gameObject.GetComponent<Collider>();
        }

        /// <summary>
        /// Performs product selection.
        /// TODO: implement visual selection marking.
        /// </summary>
        private void OnMouseUp()
        {
            if (Selection.Find().SelectionState == SelectionState.SelectProduct)
            {
                m_collider.enabled = false; //Disable collider to prevent movind interruption
                Selection.Find().SelectProduct(this);
            }
        }

        /// <summary>
        /// Deselect product. Called when placing process finished. Enables collider for being possible to be selected in future.
        /// TODO: implement visual selection marking.
        /// </summary>
        public void Deselect()
        {
            m_collider.enabled = true;
        }
    }
}
