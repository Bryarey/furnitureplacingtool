﻿using UnityEngine;
using System.Collections;


namespace FurniturePlacingTool
{
    /// <summary>
    /// Our room. It gets free-form plan, turns it into mesh floor, and creates walls for it.
    /// </summary>
    public class Room : MonoBehaviour
    {
        #region PrivateMembers

        //Room parameters:
        [SerializeField]
        private float m_roomLength;
        [SerializeField]
        private float m_roomWidth;
        [SerializeField]
        private float m_roomHeight;

        //Cached entities:
        [SerializeField]
        private MeshFilter m_meshFilter;
        [SerializeField]
        private GameObject wallPrefab;
        [SerializeField]
        private GameObject m_cameraTarget;

        //There will be stored room plan (ordered array of points)
        private Vector3[] m_planPoints;

        //static instance for quick find
        private static Room m_instance;

        private static Vector3[] m_drawedPlan;

        #endregion

        #region PublicProperties

        public Vector3[] PlanPoints
        {
            get
            {
                return m_planPoints;
            }

            set
            {
                m_planPoints = value;
            }
        }

        public static Vector3[] DrawedPlan
        {
            get
            {
                return m_drawedPlan;
            }

            set
            {
                m_drawedPlan = value;
            }
        }

        #endregion

        //Quick find
        public static Room Find()
        {
            return m_instance;
        }

        #region Initialization

        private void Awake()
        {
            m_instance = this;
        }

        private void Start()
        {
            GetPlan();
            CreateFloor();
            CreateWalls();
            SetCamera();

            if (m_drawedPlan != null)
            {
                Debug.Log("drawed plan: " + m_drawedPlan.Length);
            } else
            {
                Debug.Log("There is no drawed plan" );
            }
        }

        /// <summary>
        /// Get room plan from somewhere
        /// </summary>
        private void GetPlan()
        {
            if (m_drawedPlan != null && m_drawedPlan.Length > 2)
            {
                //Check for CW/CCW (important for normals):

                int indexOfMaxZPoint = 0;
                float maxZ = Mathf.NegativeInfinity;

                PlanPoints = new Vector3[m_drawedPlan.Length];
                for (int i = 0; i < m_drawedPlan.Length; i++)
                {
                    if (m_drawedPlan[i].z > maxZ)
                    {
                        maxZ = m_drawedPlan[i].z;
                        indexOfMaxZPoint = i;
                    }

                    PlanPoints[i] = m_drawedPlan[i] * 0.02f;
                }

                int previousIndex = indexOfMaxZPoint - 1;
                int nextIndex = indexOfMaxZPoint + 1;
                if (previousIndex < 0)
                { previousIndex = PlanPoints.Length - 1; }
                if (nextIndex >= PlanPoints.Length )
                { nextIndex = 0; }

                if (PlanPoints[previousIndex].x < PlanPoints[nextIndex].x)
                {
                    //CW! need reordering
                    for (int i = 0; i < m_drawedPlan.Length; i++)
                    {
                        PlanPoints[i] = m_drawedPlan[m_drawedPlan.Length - 1 - i] * 0.02f;
                    }
                }
            }
            else
            {
                ///Just create one:
                PlanPoints = new Vector3[5];
                PlanPoints[0] = new Vector3(0f, 0f, 0f);
                PlanPoints[1] = new Vector3(m_roomWidth, 0f, 0f);
                PlanPoints[2] = new Vector3(m_roomWidth * 1.5f, 0f, m_roomLength / 2);
                PlanPoints[3] = new Vector3(m_roomWidth, 0f, m_roomLength);
                PlanPoints[4] = new Vector3(0f, 0f, m_roomLength);
            }
        }

        /// <summary>
        /// Build floor mesh
        /// </summary>
        private void CreateFloor()
        {
            //Make array of Vector2 for triangulator:
            Vector2[] plan = new Vector2[PlanPoints.Length];
            for (int i = 0; i < PlanPoints.Length; i++)
            {
                plan[i] = new Vector2(PlanPoints[i].x, PlanPoints[i].z);
            }

            //Call builder:
            m_meshFilter.mesh = BuildFlatMesh(plan, Vector3.up);
        }

        /// <summary>
        /// Create wall objects
        /// </summary>
        private void CreateWalls()
        {
            //Iterate plan points
            for (int i = 0; i < PlanPoints.Length; i++)
            {
                //cycle array indexes
                int nextIndex = i + 1;
                if (nextIndex >= PlanPoints.Length)
                { nextIndex = 0; }

                //set up array of rectangular wall points. Look out for proper points order!
                Vector3[] newWallPoints = new Vector3[4];
                newWallPoints[0] = PlanPoints[i];
                newWallPoints[1] = PlanPoints[nextIndex];
                newWallPoints[2] = PlanPoints[nextIndex] + (Vector3.up * m_roomHeight);
                newWallPoints[3] = PlanPoints[i] + (Vector3.up * m_roomHeight);


                //Create mesh
                Mesh newWallMesh = BuildRectangle(newWallPoints);

                // setup new wall game object
                GameObject newWall = Instantiate(wallPrefab, Vector3.zero, Quaternion.identity) as GameObject;

                //Setup wall behaviour
                InteractiveWall wallBehaviour = newWall.AddComponent<InteractiveWall>();
                Edge[] wallCorners = new Edge[2];
                wallCorners[0] = new Edge(newWallPoints[0], newWallPoints[3], i);
                wallCorners[1] = new Edge(newWallPoints[1], newWallPoints[2], nextIndex);
                wallBehaviour.Corners = wallCorners;

                wallBehaviour.MeshFilter = newWall.GetComponent<MeshFilter>() as MeshFilter;
                wallBehaviour.MeshFilter.mesh = newWallMesh;
                wallBehaviour.MeshFilter.mesh.RecalculateNormals();
                wallBehaviour.MeshFilter.mesh.RecalculateBounds();

                wallBehaviour.MeshCollider = newWall.AddComponent<MeshCollider>();
            }
        }

        /// <summary>
        /// Set up camera orbit (set bounds center of new mesh as camera target)
        /// </summary>
        private void SetCamera()
        {
            m_cameraTarget.transform.position = gameObject.GetComponent<MeshRenderer>().bounds.center;
            Camera.main.GetComponent<MouseOrbitCSHARP>().ProcessCamera();
        }
        #endregion

        #region Interaction

  

        private void Update()
        {
            //Cast ray:
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                //Is pointer over wall
                InteractiveWall hitWall = hit.collider.gameObject.GetComponent<InteractiveWall>();
                if (hitWall != null)
                {
                    Vector3 pointOnWall = hit.point;
                    Edge nearestCorner = hitWall.GetNearestCorner(pointOnWall);
                    Selection.Find().Select(nearestCorner);
                    Selection.Find().CurrentMousePosition = hit.point;
                }

            }
            else
            {
                Selection.Find().DeselectEdge();
            }
        }

        #endregion

        #region MeshCreation

        /// <summary>
        /// Build simple rectangular mesh from 4 points. Points must be ordered as cycle.
        /// </summary>
        /// <param name="points">Vector3 points array</param>
        /// <returns>built mesh</returns>
        private Mesh BuildRectangle(Vector3[] points)
        {
            int[] triangles = new int[6];

            //Manual triangulation:
            triangles[0] = 0;
            triangles[1] = 1;
            triangles[2] = 2;

            triangles[3] = 0;
            triangles[4] = 2;
            triangles[5] = 3;

            Mesh mesh = new Mesh();
            mesh.vertices = points;
            mesh.triangles = triangles;

            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }

        /// <summary>
        /// Builds free-form flat mesh. Uses Unity Wiki`s triangulator class.
        /// </summary>
        /// <param name="plan">cycle-ordered array of Vector2 points.</param>
        /// <param name="normal">normal (up for floor)</param>
        /// <returns>built mesh</returns>
        private Mesh BuildFlatMesh(Vector2[] plan, Vector3 normal)
        {
            // Triangulator is a class from Unity wiki, that allows us to convert any flat free-form shape (without holes) into mesh.
            Triangulator triangulator = new Triangulator(plan);
            int[] indices = triangulator.Triangulate();

            // Create the Vector3 vertices
            Vector3[] vertices = new Vector3[plan.Length];

            //Apply normal:
            for (int i = 0; i < vertices.Length; i++)
            {
                //forward
                if (normal.z != 0)
                {
                    vertices[i] = new Vector3(plan[i].x, plan[i].y * normal.z, 0);
                }
                //left
                if (normal.x != 0)
                {
                    vertices[i] = new Vector3(0, plan[i].x, plan[i].y * normal.x);
                }
                //up
                if (normal.y != 0)
                {
                    vertices[i] = new Vector3(plan[i].x, 0, plan[i].y * normal.y);
                }
            }

            // Create the mesh
            Mesh mesh = new Mesh();
            mesh.vertices = vertices;
            mesh.triangles = indices;
            mesh.RecalculateNormals();
            mesh.RecalculateBounds();

            return mesh;
        }

        #endregion
    }

}