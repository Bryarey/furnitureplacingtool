﻿using UnityEngine;
using UnityEngine.UI;


namespace FurniturePlacingTool
{
    /// <summary>
    /// Represents screen-space UI
    /// </summary>
    public class UI : MonoBehaviour
    {
        #region PrivateMembers

        //UI elements
        [SerializeField]
        private InputField m_inputField;
        [SerializeField]
        private Text m_stateLabel;
        [SerializeField]
        private Text m_valueRangeLabel;

        //InputField using flag
        private bool m_enteringSize = false;

        //quick find
        static private UI m_instance;

        #endregion

        #region PublicProperties

        public InputField InputField
        {
            get
            {
                return m_inputField;
            }

            set
            {
                m_inputField = value;
            }
        }

        public bool EnteringSize
        {
            get
            {
                return m_enteringSize;
            }

            set
            {
                m_enteringSize = value;
            }
        }

        #endregion

        /// <summary>
        /// quick find
        /// </summary>
        /// <returns></returns>
        public static UI Find()
        {
            return m_instance;
        }

        #region MonoBehaviourMessages

        private void Awake()
        {
            m_instance = this;
        }

        #endregion

        #region EventTriggersCallbacks

        /// <summary>
        /// called on start entering value into InputField
        /// </summary>
        public void StartTyping()
        {
            m_enteringSize = true;
        }

        /// <summary>
        /// Called when inputField submits value.
        /// </summary>
        public void TypingDone()
        {
            m_enteringSize = false;
            float result;
            if (float.TryParse(m_inputField.text, out result))
            {
                Selection.Find().SetValueFromInput(result);
            }
            else
            {
                Debug.LogWarning("Incorrect value! Please enter valid decimal number...");
            }
        }

        public void DrawPlan()
        {
            Application.LoadLevel(1);
        }

        #endregion

        /// <summary>
        /// Update "state" label
        /// </summary>
        /// <param name="state"></param>
        public void UpdateState(string state)
        {
            m_stateLabel.text = state;
        }

        /// <summary>
        /// Update "range" label - min and max possible vales to input.
        /// </summary>
        /// <param name="min"></param>
        /// <param name="max"></param>
        public void UpdateRange(float min, float max)
        {
            m_valueRangeLabel.text = min.ToString() + " ... " + max.ToString();
        }

    }
}