# Furniture placement tool #

Prototype of tool, that enables user to place some stuff in some room.

### Functionality

* Select and place some objects along room walls
* Exactly measure object`s position
* Draw custom room plan
* Mouse orbit room to select view

### Usage

* Click on the green or purple stuff to select
* Click on some wall corner
* Click on another corner, that is adjacent to previous corner
* Move pointer over the wall to set correct position
* Click on input field in top-right corner of screen and type exact distance from corner to object, if you need it.
* Hold right mouse button to rotate camera
* Click button in bottom-right screen corner to draw custom shape. 


[Web-player preview] http://bryarey.zzz.com.ua/FurniturePlacementTool/FurniturePlacementTool.html